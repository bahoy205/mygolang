package models

import (
	"gorm.io/gorm"
)

type Person struct {
	Name string `json:"name"`
	Pass string `json:"pass"`
}

type User struct {
	Name     string `json:"name" validate:"required,min=3,max=32"`
	IsActive *bool  `json:"isactive" validate:"required"`
	Email    string `json:"email,omitempty" validate:"required,email,min=3,max=32"`
}

type Register struct {
	Email         string `json:"email"`
	Username      string `json:"username"`
	Password      string `json:"password"`
	Line          string `json:"line"`
	Phon          string `json:"phon"`
	Business_type string `json:"business"`
	Url           string `json:"url"`
}

type Dogs struct {
	gorm.Model        //ceate table
	Name       string `json:"name"`
	DogID      int    `json:"dog_id"`
}

type GetDogsJson struct {
	Name  string `json:"name"`
	DogID int    `json:"dog_id"`
	Type  string `json:"type"`
}

type ResultData struct {
	Data         []GetDogsJson `json:"data"`
	Name         string        `json:"name"`
	Count        int           `json:"count"`
	RedCount     int           `json:"red_count"`
	GreenCount   int           `json:"green_count"`
	PinkCount    int           `json:"pink_count"`
	NoColorCount int           `json:"no_color_count"`
}

type Company struct {
	gorm.Model        //ceate table
	Name       string `json:"name"`
	Address    string `json:"address"`
	Phone      string `json:"phone"`
	Company_ID int    `json:"com_id"`
}

type UserProfile struct {
	gorm.Model         //ceate table
	Employee_ID int    `json:"emp_id"`
	FirstName   string `json:"fname"`
	LastName    string `json:"lname"`
	Birthday    string `json:"bd"`
	Age         int    `json:"age"`
	Email       string `json:"email"`
	Tel         string `json:"tel"`
}

type GetUserProfileJson struct {
	Employee_ID int    `json:"emp_id"`
	FirstName   string `json:"fname"`
	LastName    string `json:"lname"`
	Birthday    string `json:"bd"`
	Age         int    `json:"age"`
	Email       string `json:"email"`
	Tel         string `json:"tel"`
	Type        string `json:"type"`
}

type ResultUserProfileData struct {
	Data              []GetUserProfileJson `json:"data"`
	Name              string               `json:"name"`
	Count             int                  `json:"count"`
	GenZCount         int                  `json:"gen_z"`
	GenYCount         int                  `json:"gen_y"`
	GenXCount         int                  `json:"gen_x"`
	BabyBoomerCount   int                  `json:"baby_boomer"`
	GiGenerationCount int                  `json:"generation"`
}
