package controllers

import (
	database "go-fibar-test/databese"
	"go-fibar-test/models"

	// m "go-fibar-test/models"
	"log"
	"regexp"
	"strconv"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

func HelloTest(c *fiber.Ctx) error {
	return c.SendString("Hellom World!")
}

func GetUserProfiles(c *fiber.Ctx) error {
	db := database.DBConn
	var profile []models.UserProfile

	db.Find(&profile) //delelete = null
	return c.Status(200).JSON(profile)
}
func BodyParserTest(c *fiber.Ctx) error {
	p := new(models.Person)

	if err := c.BodyParser(p); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": "cannot parse JSON",
		})
	}

	log.Println(p.Name)
	log.Println(p.Pass)
	str := p.Name + p.Pass
	return c.JSON(str)
}
func ParamsTest(c *fiber.Ctx) error {

	str := "hello ==> " + c.Params("name")
	c.Params("name")
	return c.JSON(str)
}
func QueryTest(c *fiber.Ctx) error {
	a := c.Query("search") // ถ้า Search มาจาก หน้าบ้าน จะเก็บใน a
	str := "my search is  " + a
	return c.JSON(str)
}
func ValidateTest(c *fiber.Ctx) error {
	//*Connect to database
	user := new(models.User)
	if err := c.BodyParser(&user); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": err.Error(),
		})
	}
	validate := validator.New()
	errors := validate.Struct(user)
	// * nil = null

	if errors != nil {
		return c.Status(fiber.StatusBadRequest).JSON(errors.Error())
	}

	return c.JSON(user)
}

func FactorialTest(c *fiber.Ctx) error {
	number := c.Params("number")
	fnumber, err := strconv.Atoi(number)

	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString("invalid number")
	}
	x := 1
	for i := fnumber; i > 0; i-- {
		x *= i
	}

	if fnumber < 0 {
		return c.Status(fiber.StatusBadRequest).SendString("number must be non-negative")
	}

	return c.JSON(x)
}

func Register(c *fiber.Ctx) error {
	p := new(models.Register)

	if err := c.BodyParser(p); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": "cannot parse JSON",
		})
	}

	// Validate username
	match, _ := regexp.MatchString(`^[a-zA-Z0-9_-]+$`, p.Username)
	if !match {
		return c.Status(fiber.StatusBadRequest).SendString("ชื่อผู้ใช้งานต้องประกอบด้วยตัวอักษร a-z, A-Z, ตัวเลข 0-9, หรือเครื่องหมาย _ หรือ - เท่านั้น")
	}

	// Validate password length
	if len(p.Password) < 6 || len(p.Password) > 20 {
		return c.Status(fiber.StatusBadRequest).SendString("ความยาวของรหัสผ่านต้องมากกว่า 6 และไม่เกิน 20 ตัวอักษร")
	}

	// Validate phone number length
	if len(p.Phon) < 10 || len(p.Phon) > 10 {
		return c.Status(fiber.StatusBadRequest).SendString("กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง (อย่างน้อย 10 ตัวอักษร)")
	}

	// Validate business type
	if p.Business_type == "" {
		return c.Status(fiber.StatusBadRequest).SendString("กรุณากรอกประเภทธุรกิจ")
	}

	// Validate URL
	if p.Url == "" {
		return c.Status(fiber.StatusBadRequest).SendString("กรุณากรอกชื่อเว็บไซต์")
	}

	// Log data
	log.Println(p.Username)
	log.Println(p.Password)
	log.Println(p.Line)
	log.Println(p.Phon)
	log.Println(p.Business_type)
	log.Println(p.Url)

	return c.JSON(p)
}

func AsciiTest(c *fiber.Ctx) error {
	queryParam := c.Query("tax_id")

	var result string
	var text string

	for _, v := range queryParam {
		result = strconv.Itoa(int(v))

		text += " " + result
	}
	// str := "my search is  " + queryParam
	return c.JSON(text)
}

func DogIDGreaterThan100(db *gorm.DB) *gorm.DB {
	return db.Where("dog_id > ? AND dog_id < ?", 50, 100)
}

func GetDogs(c *fiber.Ctx) error {
	db := database.DBConn
	var dogs []models.Dogs

	db.Scopes(DogIDGreaterThan100).Find(&dogs) //delelete = null
	return c.Status(200).JSON(dogs)
}

func HistoryDogs(c *fiber.Ctx) error {
	db := database.DBConn
	var dogs []models.Dogs

	result := db.Unscoped().Where("deleted_at IS NOT NULL").Find(&dogs)

	if result.Error != nil {
		return c.Status(500).JSON(fiber.Map{
			"error": "Internal Server Error",
		})
	}

	return c.Status(200).JSON(dogs)
}

func GetDog(c *fiber.Ctx) error {
	db := database.DBConn
	search := strings.TrimSpace(c.Query("search"))
	var dog []models.Dogs

	result := db.Find(&dog, "dog_id = ?", search)

	// returns found records count, equals `len(users)
	if result.RowsAffected == 0 {
		return c.SendStatus(404)
	}
	return c.Status(200).JSON(&dog)
}

func AddDog(c *fiber.Ctx) error {
	//twst3
	db := database.DBConn
	var dog models.Dogs

	if err := c.BodyParser(&dog); err != nil {
		return c.Status(503).SendString(err.Error())
	}

	db.Create(&dog)
	return c.Status(201).JSON(dog)
}

func UpdateDog(c *fiber.Ctx) error {
	db := database.DBConn
	var dog models.Dogs
	id := c.Params("id")

	if err := c.BodyParser(&dog); err != nil {
		return c.Status(503).SendString(err.Error())
	}

	db.Where("id = ?", id).Updates(&dog)
	return c.Status(200).JSON(dog)
}

func RemoveDog(c *fiber.Ctx) error {
	db := database.DBConn
	id := c.Params("id")
	var dog models.Dogs

	result := db.Delete(&dog, id)

	if result.RowsAffected == 0 {
		return c.SendStatus(404)
	}

	return c.SendStatus(200)
}

func GetDogsJson(c *fiber.Ctx) error {
	db := database.DBConn
	var dogs []models.Dogs

	db.Find(&dogs) //10ตัว

	var dataResults []models.GetDogsJson

	colorCounters := map[string]int{
		"red":      0,
		"green":    0,
		"pink":     0,
		"no color": 0,
	}
	for _, v := range dogs { //1 inet 112 //2 inet1 113
		typeStr := ""
		if v.DogID >= 10 && v.DogID <= 50 {
			typeStr = "red"
		} else if v.DogID >= 100 && v.DogID <= 150 {
			typeStr = "green"
		} else if v.DogID >= 200 && v.DogID <= 250 {
			typeStr = "pink"
		} else {
			typeStr = "no color"
		}

		colorCounters[typeStr]++

		d := models.GetDogsJson{
			Name:  v.Name,  //inet1
			DogID: v.DogID, //113
			Type:  typeStr, //green
		}
		dataResults = append(dataResults, d)
		// sumAmount += v.Amount
	}

	r := models.ResultData{
		Data:         dataResults,
		Name:         "golang-test",
		Count:        len(dogs), // Total number of dogs
		RedCount:     colorCounters["red"],
		GreenCount:   colorCounters["green"],
		PinkCount:    colorCounters["pink"],
		NoColorCount: colorCounters["no color"],
	}
	return c.Status(200).JSON(r)
}

func GetCompanys(c *fiber.Ctx) error {
	db := database.DBConn
	var company []models.Company

	db.Find(&company) //delelete = null
	return c.Status(200).JSON(company)
}

func GetCompany(c *fiber.Ctx) error {
	db := database.DBConn
	search := strings.TrimSpace(c.Query("search"))
	var company []models.Company

	result := db.Find(&company, "Company_ID = ?", search)

	// returns found records count, equals `len(users)
	if result.RowsAffected == 0 {
		return c.SendStatus(404)
	}
	return c.Status(200).JSON(&company)
}

func AddCompany(c *fiber.Ctx) error {
	//twst3
	db := database.DBConn
	var company models.Company

	if err := c.BodyParser(&company); err != nil {
		return c.Status(503).SendString(err.Error())
	}

	db.Create(&company)
	return c.Status(201).JSON(company)
}

func UpdateCompany(c *fiber.Ctx) error {
	db := database.DBConn
	var company models.Company
	id := c.Params("id")

	if err := c.BodyParser(&company); err != nil {
		return c.Status(503).SendString(err.Error())
	}

	db.Where("id = ?", id).Updates(&company)
	return c.Status(200).JSON(company)
}

func RemoveCompany(c *fiber.Ctx) error {
	db := database.DBConn
	id := c.Params("id")
	var company models.Company

	result := db.Delete(&company, id)

	if result.RowsAffected == 0 {
		return c.SendStatus(404)
	}

	return c.SendStatus(200)
}

func AddUserProfile(c *fiber.Ctx) error {
	//twst3
	db := database.DBConn
	var profile models.UserProfile

	if err := c.BodyParser(&profile); err != nil {
		return c.Status(503).SendString(err.Error())
	}

	db.Create(&profile)
	return c.Status(201).JSON(profile)
}

func UpdateUserProfile(c *fiber.Ctx) error {
	db := database.DBConn
	var profile models.UserProfile
	id := c.Params("id")

	if err := c.BodyParser(&profile); err != nil {
		return c.Status(503).SendString(err.Error())
	}

	db.Where("id = ?", id).Updates(&profile)
	return c.Status(200).JSON(profile)
}

func RemoveUserProfile(c *fiber.Ctx) error {
	db := database.DBConn
	id := c.Params("id")
	var profile models.UserProfile

	result := db.Delete(&profile, id)

	if result.RowsAffected == 0 {
		return c.SendStatus(404)
	}

	return c.SendStatus(200)
}

func GetUserProfile(c *fiber.Ctx) error {
	db := database.DBConn
	search := strings.TrimSpace(c.Query("search"))
	var profile []models.UserProfile //ประกาศตัวแปร profile โดนจะนำ models.UserProfile ด้วย

	// result := db.Where("emp_id = ? OR fname = ? OR lname = ?", search, search, search).Find(&profile)
	result := db.Find(&profile, "employee_id = ? OR first_name = ? OR last_name = ?", search, search, search) //ส่วนนี้คือการ Where DATA เพื่อที่จะหา emp_id, fname, lname จาก Database

	if result.RowsAffected == 0 {
		return c.SendStatus(404)
	}
	return c.Status(200).JSON(&profile)

}

func GetUserProfileJson(c *fiber.Ctx) error {
	db := database.DBConn
	var profile []models.UserProfile //ประกาศตัวแปร profile โดยจะนำ models.UserProfile ด้วย

	db.Find(&profile) //10ตัว

	var dataResults []models.GetUserProfileJson //ประกาศตัวแปร profile โดยจะนำ models.GetUserProfileJson ด้วย

	generation := map[string]int{ //กำหนดให้ genetation โดยการ map string เพื่อเก็บ ค่าที่ได้การจาก loop Age และนำผลมา loop ที่เป็น string มาเก็บไว้ใน map
		"GenZ":            0,
		"GenY":            0,
		"GenX":            0,
		"Baby Boomer":     0,
		"G.I. Generation": 0,
	}
	for _, v := range profile {
		group := ""
		if v.Age < 24 {
			group = "GenZ"
		} else if v.Age >= 24 && v.Age <= 41 {
			group = "GenY"
		} else if v.Age >= 42 && v.Age <= 56 {
			group = "GenX"
		} else if v.Age >= 57 && v.Age <= 75 {
			group = "Baby Boomer"
		} else {
			group = "G.I. Generation"
		}

		generation[group]++ //เมื่อได้ผลตามเงื่อนไขแล้วมา generation + 1 เพื่อเป็นการเก็บค่า generation

		d := models.GetUserProfileJson{
			FirstName:   v.FirstName,
			LastName:    v.LastName,
			Birthday:    v.Birthday,
			Age:         v.Age,
			Email:       v.Email,
			Tel:         v.Tel,
			Employee_ID: v.Employee_ID,
			Type:        group,
		}
		dataResults = append(dataResults, d)
		// sumAmount += v.Amount
	}

	r := models.ResultUserProfileData{
		Data:              dataResults,
		Name:              "golang-test",
		Count:             len(profile),
		GenZCount:         generation["GenZ"],
		GenYCount:         generation["GenY"],
		GenXCount:         generation["GenX"],
		BabyBoomerCount:   generation["Baby Boomer"],
		GiGenerationCount: generation["G.I. Generation"],
	}
	return c.Status(200).JSON(r)
}
