package routes

import (
	c "go-fibar-test/controllers"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/basicauth"
)

func InetRoutes(app *fiber.App) {
	api := app.Group("/api")
	v1 := api.Group("/v1")

	profile := v1.Group("/profile")

	profile.Get("", c.GetUserProfiles)

	app.Use(basicauth.New(basicauth.Config{
		Users: map[string]string{
			"tar":     "1234",
			"tata":    "1234",
			"gofiber": "256456465",
			"testgo":  "23012023",
		},
	}))
	// api := app.Group("/api")
	// // /api/v1
	// v1 := api.Group("/v1")

	// controllers = c

	v3 := api.Group("/v3")
	v1.Get("/", c.HelloTest)

	v1.Post("/", c.BodyParserTest)

	v1.Get("/user/:name", c.ParamsTest)
	v1.Post("/inet", c.QueryTest)

	v1.Post("/valid", c.ValidateTest)

	v1.Post("/fact/:number", c.FactorialTest)

	v1.Post("/register", c.Register)

	v3.Post("/tar", c.AsciiTest)

	//CRUD dogs
	dog := v1.Group("/dog")
	dog.Get("", c.GetDogs)
	dog.Get("/filter", c.GetDog)
	dog.Get("/json", c.GetDogsJson)
	dog.Post("/", c.AddDog)
	dog.Put("/:id", c.UpdateDog)
	dog.Delete("/:id", c.RemoveDog)
	dog.Get("/History", c.HistoryDogs)

	//CRUD company
	company := v1.Group("/company")
	company.Get("", c.GetCompanys)
	company.Get("/filter", c.GetCompany)
	company.Post("/", c.AddCompany)
	company.Put("/:id", c.UpdateCompany)
	company.Delete("/:id", c.RemoveCompany)

	// profile := v1.Group("/profile")
	profile.Get("", c.GetUserProfiles)
	profile.Get("/filter", c.GetUserProfile)
	profile.Get("/json", c.GetUserProfileJson)
	profile.Post("/", c.AddUserProfile)
	profile.Put("/:id", c.UpdateUserProfile)
	profile.Delete("/:id", c.RemoveUserProfile)
	// profile.Get("/History", c.HistoryDogs)

}
